import signal
import unittest
from unittest.mock import MagicMock, patch

from tangogql.aioserver import (
    ensure_connectivity,
    setup,
    setup_logger,
    signal_handler,
)


class TestServer(unittest.TestCase):

    @patch.dict("sys.modules", {"PyTango": MagicMock()})
    @patch("tangogql.aioserver.logger")
    @patch("tangogql.aioserver.sys.exit")
    def test_ensure_connectivity_success(self, mock_exit, mock_logger):
        from PyTango import Database

        Database.return_value = MagicMock()
        ensure_connectivity(retries=3, sleep_duration=1)
        self.assertTrue(Database.called)
        mock_exit.assert_not_called()

    @patch("tangogql.aioserver.yaml.safe_load")
    @patch("tangogql.aioserver.logging.config.dictConfig")
    @patch("tangogql.aioserver.logging.getLogger")
    def test_setup_logger_with_config_file(
        self, mock_getLogger, mock_dictConfig, mock_safe_load
    ):
        mock_logger = MagicMock()
        mock_getLogger.return_value = mock_logger
        mock_safe_load.return_value = {"version": 1}

        with patch("builtins.open", unittest.mock.mock_open(read_data="data")):
            logger = setup_logger()

        self.assertEqual(logger, mock_logger)
        self.assertTrue(mock_dictConfig.called)

    @patch("tangogql.aioserver.logging.basicConfig")
    @patch("tangogql.aioserver.logging.getLogger")
    def test_setup_logger_without_config_file(
        self, mock_getLogger, mock_basicConfig
    ):
        mock_logger = MagicMock()
        mock_getLogger.return_value = mock_logger

        logger = setup_logger(default_path="non_existent.yaml")

        self.assertEqual(logger, mock_logger)
        self.assertTrue(mock_basicConfig.called)

    @patch("tangogql.aioserver.setup_logger")
    @patch("tangogql.aioserver.setup_server")
    def test_setup(self, mock_setup_server, mock_setup_logger):
        mock_logger = MagicMock()
        mock_app = MagicMock()
        mock_setup_logger.return_value = mock_logger
        mock_setup_server.return_value = mock_app

        logger, app = setup()

        self.assertEqual(logger, mock_logger)
        self.assertEqual(app, mock_app)
        self.assertTrue(mock_setup_logger.called)
        self.assertTrue(mock_setup_server.called)

    @patch("tangogql.aioserver.asyncio.get_event_loop")
    @patch("tangogql.aioserver.logger")
    def test_signal_handler(self, mock_logger, mock_get_event_loop):
        mock_loop = MagicMock()
        mock_get_event_loop.return_value = mock_loop

        signal_handler(signal.SIGTERM)

        mock_logger.info.assert_called_with(
            "Received %s, stopping the server", signal.SIGTERM
        )
        mock_loop.stop.assert_called_once()


if __name__ == "__main__":
    unittest.main()
